#include <iostream>
#include "sqlite3.h"
#include <string>
#include <unordered_map>
#include <vector>
#include <stdlib.h> 

using namespace std;

unordered_map<string, vector<string>> results;

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);

void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

int callback(void* ret, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	// Create DB
	rc = sqlite3_open("carsDealer.db", &db);
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	clearTable();

	// 2A
	cout << carPurchase(1, 2, db, zErrMsg) << endl; // won't work
	cout << carPurchase(10, 4, db, zErrMsg) << endl; // work
	cout << carPurchase(12, 23, db, zErrMsg) << endl; // work
	// 2B
	cout << balanceTransfer(1, 2, 6000, db, zErrMsg) << endl;

	system("PAUSE");
	sqlite3_close(db);
	return 0;
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int rc;
	string query("SELECT * FROM accounts JOIN cars WHERE accounts.id =  " + to_string(buyerid) + " AND cars.id = " + to_string(carid) + " AND accounts.balance >= cars.price AND cars.available = 1");

	// Change the name of the last inserted people
	rc = sqlite3_exec(db, query.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	if (results.begin() == results.end())
		return false;

	query = "begin transaction; UPDATE cars SET available = 0 WHERE id = " + to_string(carid) + "; UPDATE accounts SET balance = 0 WHERE id =" + to_string(buyerid) + "; commit;";
	// Change the name of the last inserted people
	rc = sqlite3_exec(db, query.c_str(), 0, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	return true;
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc;
	string query("begin transaction; UPDATE accounts SET balance = balance - " + to_string(amount) + " WHERE id = " + to_string(from) + "; " +
		"UPDATE accounts SET balance = balance + " + to_string(amount) + " WHERE id = " + to_string(to) + "; " + 
		"commit;");
	
	// Change the name of the last inserted people
	rc = sqlite3_exec(db, query.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	return true;
}